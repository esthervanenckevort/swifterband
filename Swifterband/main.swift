//
//  main.swift
//  Swifterband
//
//  Created by David van Enckevort on 27-04-18.
//  Copyright © 2018 All Things Digital. All rights reserved.
//

import Foundation

var parts = [URLSession.MultiPartFormData]()

let url = URL(string: "http://www.csm-testcenter.org/test")!

guard let fileUpload = "A test file as binary data.\r\n".data(using: .ascii) else {
    fatalError("Failed to encode data.")
}
var file = URLSession.MultiPartFormData(name: "file_upload", filename: "test.txt", data: fileUpload, encoding: .data)
parts.append(file)

guard let text = "Some data as plain text.".data(using: .utf8) else {
    fatalError("Failed to encode text")
}
var data = URLSession.MultiPartFormData(name: "data", filename: nil, data: text, encoding: .plain)
parts.append(data)

let session = URLSession.shared
let semaphore = DispatchSemaphore(value: 0)
let task = session.upload(to: url, from: parts) {
    data, response, error in
    if let error = error {
        print ("error: \(error)")
        return
    }
    guard let response = response as? HTTPURLResponse,
        (200...299).contains(response.statusCode) else {
            print ("server error")
            return
    }
    if let data = data, let dataString = String(data: data, encoding: .utf8) {
        print ("got data: \(dataString)")
    }
    semaphore.signal()

    }

task.resume()
semaphore.wait()
