//
//  URLSession+Extensions.swift
//  Swifterband
//
//  Created by David van Enckevort on 27-04-18.
//  Copyright © 2018 All Things Digital. All rights reserved.
//

import Foundation

extension URLSession {
    struct MultiPartFormData {
        var name: String
        var filename: String?
        var data: Data
        var encoding: ContentType
    }

    enum HttpHeaderField: String {
        case contentType = "Content-Type"
        case contentDisposition = "Content-Disposition"
    }

    enum ContentType: String {
        case json = "application/json"
        case formData = "multipart/form-data"
        case plain = "text/plain"
        case html = "text/html"
        case data = "application/octet-stream"
        case urlencoded = "application/x-www-form-urlencoded"
    }

    private func composeBody(from parts: [MultiPartFormData], with request: inout URLRequest) -> Data {
        request.httpMethod = "POST"
        let boundary = "\(UUID())"
        request.addValue("\(ContentType.formData.rawValue); ; boundary=\(boundary)", forHTTPHeaderField: HttpHeaderField.contentType.rawValue)
        let lineSeparator = "\r\n".data(using: .ascii)!
        let partsSeparator = "--\(boundary)".data(using: .ascii)!
        var data = Data()
        for part in parts {
            data.append(partsSeparator)
            data.append(lineSeparator)
            data.append("\(HttpHeaderField.contentDisposition.rawValue): form-data; name=\"\(part.name)\"".data(using: .ascii)!)
            if let filename = part.filename,
                let fileField = " filename=\"\(filename)\"".data(using: .ascii) {
                data.append(fileField)
            }
            data.append(lineSeparator)
            data.append("\(HttpHeaderField.contentType.rawValue): \(part.encoding.rawValue)".data(using: .ascii)!)
            data.append(lineSeparator)
            data.append(lineSeparator)
            data.append(part.data)
        }
        data.append(lineSeparator)
        data.append("--\(boundary)--".data(using: .ascii)!)
        data.append(lineSeparator)
        return data
    }

    func upload(to url: URL, from parts: [MultiPartFormData], completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionUploadTask {
        var request = URLRequest(url: url)
        let data = composeBody(from: parts, with: &request)
        return URLSession.shared.uploadTask(with: request, from: data, completionHandler: completionHandler)
    }
}
